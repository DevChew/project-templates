const serverConfig = require('./config/webpack/server');
const clientConfig = require('./config/webpack/browser');

module.exports = [ serverConfig, clientConfig ];
