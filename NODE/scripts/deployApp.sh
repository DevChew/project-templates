#!/bin/bash

DIR="$( cd "$(dirname "$0")" ; pwd -P )" #this file should be inside tmp/$buildnumber

APP_ROOT="$( cd $DIR; cd ../../../public_nodejs; pwd -P)"
DEPLOY_ROOT="$( cd $DIR; cd ../dist; pwd -P)"

echo 'remove "__old" app folder'
rm -rf ${APP_ROOT}__old

echo 'copy staging app'
mv ${DEPLOY_ROOT} ${APP_ROOT}__staging

echo 'rename existing app to __old if exist'
mv ${APP_ROOT} ${APP_ROOT}__old

echo 'rename __staging to normal'
mv ${APP_ROOT}__staging ${APP_ROOT}