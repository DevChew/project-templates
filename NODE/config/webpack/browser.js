module.exports = {
  target: 'web',
  entry: './src/browser/index.js',
  output: {
    path: path.resolve(__dirname, 'dist/browser'),
    filename: 'app.js'
  }
};