# PHP/Apache Siete template

## Requirements

* docker and docker-compose
* bash shell with curl and zip/unzip

## Development

run docker

`docker-compose up`

open browser, links on bottom of page

## Import Database

On first run, or when you rebuild your container, or just to clear any changes, you can import clean datbase, to do that, you need to:

After docker-compose up, run `. scripts/importDatabase.sh`

## Deployment

This template include bitbucket pipeline, each push to master trigger deploy to master

You can trigger manual pipeline, and deploy to beta from branch

Each deployment is working done in several steps:

1. pack everything from src folder
1. push package to server
1. unzip package to new foldedr ( folder name is taken from build number)
1. update htaccess file, and route to new release folder

### Folder structure

```
    |-- .htaccess.template
    |-- bitbucket-pipelines.yml
    |-- docker-compose.yaml
    |-- README.md <- You are here
    |-- .docker
    |   |-- PHP
    |       |-- Dockerfile
    |-- data
    |   |-- database.sql <- Dummy database
    |-- scripts
    |   |-- importDatabase.sh <- Run this to recreate database
    |-- src <-- Your app
```

### Server files structure

When files are deployed to sever, few thing happen:

```
|-- Server deploy root
    |-- .htaccess <- this file route trafic to newset release
    |-- 75
        ^^ folder with release in this case 75, each release gets new folder based on build number,
        content from this folder is copied from src folder
```

## Links

| Site | link |
|---|---|
| Local | https://localhost:8080/ |
| Local phpmydamin | https://localhost:8181/ |
| --- | --- |
| Beta | https://beta.example.com |
| --- | --- |
| Prod | https://example.com/ |
