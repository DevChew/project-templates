SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `karma` int(11) NOT NULL DEFAULT '0',
  `approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

INSERT INTO `test` (`ID`, `post_ID`, `author`, `author_email`, `author_url`, `author_IP`, `date`, `date_gmt`, `content`, `karma`, `approved`, `agent`, `type`, `parent`, `user_id`) VALUES
(1, 1, 'A example Commenter', 'wapuu@example.example', 'https://example.org/', '', '2019-06-29 14:42:46', '2019-06-29 14:42:46', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);
