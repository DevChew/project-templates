<html>
<head><?php wp_head(); ?></head>
<body>
<?php
if ( have_posts() ) {
    while ( have_posts() ) {
        the_post();
        the_title( '<h3>', '</h3>' );
        the_content();
        echo '<hr />';
        echo '<h3>All Post Data</h3>';
        echo '<pre>';
        echo var_export($GLOBALS['post'], TRUE);
        echo '</pre>';
        
        echo '<hr />';
        echo '<h3>All Post Meta</h3>';
        echo '<pre>';
        echo var_export(get_post_custom(), TRUE);
        echo '</pre>';
        echo '<hr />';
    }
}
wp_footer();
?>
</body>
</html>