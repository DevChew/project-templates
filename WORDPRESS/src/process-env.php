<?php
function process_env($name) {
    $file = dirname(__FILE__) . '/envs.json';
    if (file_exists($file)){
        $string = file_get_contents($file);
        $ENVS=json_decode($string,true);
        return $ENVS[$name];
    }
    return getenv($name);
}
?>