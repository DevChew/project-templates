<?php
require_once(dirname( __FILE__ ) . '/process-env.php');
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', process_env('DB_NAME') );

/** MySQL database username */
define( 'DB_USER', process_env('DB_USER') );

/** MySQL database password */
define( 'DB_PASSWORD', process_env('DB_PASSWORD') );

/** MySQL hostname */
define( 'DB_HOST', process_env('DB_HOST') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', process_env('AUTH_KEY') );
define( 'SECURE_AUTH_KEY', process_env('SECURE_AUTH_KEY') );
define( 'LOGGED_IN_KEY', process_env('LOGGED_IN_KEY') );
define( 'NONCE_KEY', process_env('NONCE_KEY') );
define( 'AUTH_SALT', process_env('AUTH_SALT') );
define( 'SECURE_AUTH_SALT', process_env('SECURE_AUTH_SALT') );
define( 'LOGGED_IN_SALT', process_env('LOGGED_IN_SALT') );
define( 'NONCE_SALT', process_env('NONCE_SALT') );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = process_env('DATABASE_PREFIX');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', process_env('ENVIROMENT') == 'development' );

/**
 * only for convinence
 */
$appAddress = 'https://' . $_SERVER['SERVER_NAME'];
if (process_env('ENVIROMENT') == 'development') {
    $appAddress = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
}

/**
 * Set Custom Content Directory
 */
define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/app' );
define( 'WP_CONTENT_URL', $appAddress . '/app');

/**
 * disable all updates
 */
if (process_env('ENVIROMENT') !== 'development') {
        define( 'AUTOMATIC_UPDATER_DISABLED', true );
        define( 'DISALLOW_FILE_MODS', true );
}

/**
 * Security tweaks
 */
define( 'FORCE_SSL_ADMIN', process_env('ENVIROMENT') !== 'development' );

/**
 * Server size optimalization
 */
define( 'IMAGE_EDIT_OVERWRITE', true );
define( 'WP_SITEURL', $appAddress . '/admin' );
define( 'WP_HOME', $appAddress );


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );