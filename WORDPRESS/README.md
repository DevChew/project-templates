# Wordpress Siete template

## Requirements

* docker and docker-compose
* bash shell with curl and zip/unzip

## Development

create `.env` file bsed on `.env.example` example

only on first run, or if you want to change wordpress version

`. scripts/bootstrap.sh`

run docker

`docker-compose up`

open browser, links on bottom of page

## Import Database

On first run, or when you rebuild your container, or just to clear any changes, you can import clean datbase, to do that, you need to:

After docker-compose up, run `. scripts/importDatabase.sh`

## Setup

### Envs

See below for an explanation of each configuration option used within the .env file.

* **ENVIROMENT** - set to `development` to enable debug and install and update plugins
* **WP_VERSION** - version of wordpress
* **WORDPRESS_CORE_PATH** - path to wordpress core files, you will ned to login with this path `https://example.com/{WORDPRESS_CORE_PATH}/wp-admin`, if you change this, remeber to update gitignore file
* **DB_NAME** - database name, the same as in wp-config
* **DB_USER** - database user, the same as in wp-config
* **DB_PASSWORD** - databse password, the same as in wp-config
* **DB_HOST** - database host, the same as in wp-config
* **DATABASE_PREFIX** - database prefix, the same as in wp-config

 You can generate these using the [WordPress.org secret-key service](https://api.wordpress.org/secret-key/1.1/salt/)

* **AUTH_KEY** - the same as in wp-config
* **SECURE_AUTH_KEY** - the same as in wp-config
* **LOGGED_IN_KEY** - the same as in wp-config
* **NONCE_KEY** - the same as in wp-config
* **AUTH_SALT** - the same as in wp-config
* **SECURE_AUTH_SALT** - the same as in wp-config
* **LOGGED_IN_SALT** - the same as in wp-config
* **NONCE_SALT** - the same as in wp-config

### Wordpress

#### import wordpress to use with this template

* move wp-contet folder content to app folder
* set env variables based on yout original wp-config, see [Setup>Envs](#Envs)
* change wordperss address to by like this `https://your-address.com/admin`
    * login to your database
    * in table wp_options change siteurl to `https://your-address.com/admin`
    * in same table check if your home is set to `https://your-address.com`
* configure uploads folder
    * rename wp-uploads to uploads
    * on development leave folder as it is, on server move uploads folder to root of your application see [### Server files structure](#Server-files-structure)
    * login to admin panel [#Links](#Links)
    * go to `admin/wp-admin/options.php` and set `upload_path` and `upload_url_path` to be empty
    * go to `admin/wp-admin/options-media.php` and set and reset use year month folder structure options, this should reset uploads config
    * 
## Deployment

This template include bitbucket pipeline, each push to master trigger deploy to master

You can trigger manual pipeline, and deploy to beta from branch

Each deployment is done in several steps:

1. download fresh wordpress archive
2. move wordpress core to folder described by env variable
3. pack everything from src folder
4. push package to server
5. unzip package to new foldedr ( folder name is taken from build number)
6. link uploads folder in root folder with one in new release
7. update htaccess file, and route to new release folder

### Folder structure

```
    |-- .env <- You need to create this file
    |-- .env-example
    |-- .htaccess.template
    |-- bitbucket-pipelines.yml
    |-- docker-compose.yaml
    |-- README.md <- You are here
    |-- .docker
    |   |-- PHP
    |       |-- Dockerfile
    |-- data
    |   |-- database.sql <- Dummy database
    |-- scripts
    |   |-- bootstrap.sh <- Run this script to bootstrap local enviroment
    |   |-- importDatabase.sh <- Run this to recreate database
    |   |-- build.sh <- user in pipeline
    |   |-- storeEnv.sh <- user in pipeline
    |-- src
        |-- index.php <- modified default wordpress index file
        |-- process-env.php <- helper to read env variables
        |-- wp-config.php <- wordpress config file
        |-- admin <- this folder contains all wordpress core files
        |-- app <- your app lives here ( before wp-content )
        |   |-- index.php
        |   |-- plugins
        |   |   |-- hello.php
        |   |   |-- index.php
        |   |-- themes
        |       |-- index.php
        |       |-- minimum
        |           |-- index.php
        |           |-- style.css
        |-- uploads <- all uploads are store here (this file must be ignored, and not be fart of deploy package)
```

### Server files structure

When files are deployed to sever, few thing happen:

```
|-- Server deploy root
    |-- .htaccess <- this file route trafic to newset release
    |-- uploads
        ^^ Folder with all uploads (presisted, will not be removed or overwrited on new release)
    |-- 75
        ^^ folder with release in this case 75, each release gets new folder based on build number
        |-- admin
        |-- app
        |-- uploads <- this is not a folder, every deploy create symlink, connecting this folder, with upload folder on root (../uploads)
        |-- index.php <- nothing change here
        |-- process-env.php <- nothing change here
        |-- wp-config.php <- nothing change here
        |-- envs.json <- this file is created post deploy, its stores all env variable neccessary to run application ( see storeEnh.sh )
```

## Update wordpress addess via sql

```
UPDATE wp_options SET option_value = replace(option_value, 'old.address.com', 'new.address.com') WHERE option_name = 'home' OR option_name = 'siteurl';
UPDATE wp_posts SET post_content = replace(post_content, 'old.address.com', 'new.address.com');
UPDATE wp_postmeta SET meta_value = replace(meta_value,'old.address.com','new.address.com');
UPDATE wp_usermeta SET meta_value = replace(meta_value, 'old.address.com','new.address.com');
UPDATE wp_links SET link_url = replace(link_url, 'old.address.com','new.address.com');
UPDATE wp_comments SET comment_content = replace(comment_content , 'old.address.com','new.address.com');
```

## Links

| Site | link |
|---|---|
| Local | https://localhost:8080/ |
| Local phpmydamin | https://localhost:8181/ |
| Local Wordpres Admin | https://localhost:8080/admin/wp-login.php |
| --- | --- |
| Beta | https://beta.example.com |
| Beta Wordpress | https://beta.example.com/admin/wp-login.php |
| --- | --- |
| Prod | https://example.com/ |
| Prod Wordpress | https://example.com/admin/wp-login.php |

> Local Wordpres: Login `Test` Password `Test`