#!/bin/bash

# $1 is build number passed as a rgument in script execution
DIR="$( cd "$(dirname "$0")" ; pwd -P )"
BUILDNUMBER=$1

unzip $DIR/package.zip -q -d $DIR/$BUILDNUMBER
rm $DIR/package.zip
rm $DIR/.htaccess || true
sed s/{{{folder}}}/$BUILDNUMBER/g $DIR/.htaccess.template > $DIR/.htaccess
rm $DIR/.htaccess.template || true
mkdir -p $DIR/uploads
ln -s $DIR/uploads/ $DIR/$BUILDNUMBER/app

# self destruct
rm $0