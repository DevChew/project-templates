echo 'Download Wordpress $WP_VERSION';

curl https://wordpress.org/wordpress-$WP_VERSION.zip --output wordpress.zip

echo 'extract wordpress to $WORDPRESS_CORE_PATH';

rm -rf ./src/$WORDPRESS_CORE_PATH/
mkdir ./src/$WORDPRESS_CORE_PATH/
mkdir ./temp
unzip -q wordpress.zip -d ./temp/

echo 'Move folder'

mv ./temp/wordpress/* ./src/$WORDPRESS_CORE_PATH

echo 'remove config files from $WORDPRESS_CORE_PATH'

rm ./src/$WORDPRESS_CORE_PATH/index.php
rm ./src/$WORDPRESS_CORE_PATH/wp-config-sample.php
rm -rf ./src/$WORDPRESS_CORE_PATH/wp-content

echo 'cleanup'

rm -rf ./temp
rm ./wordpress.zip
